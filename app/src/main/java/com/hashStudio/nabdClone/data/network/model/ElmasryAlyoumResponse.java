package com.hashStudio.nabdClone.data.network.model;

import java.util.ArrayList;
import java.util.List;

public class ElmasryAlyoumResponse {


        Rss RssObject;


        // Getter Methods

        public Rss getRss() {
            return RssObject;
        }

        // Setter Methods

        public void setRss(Rss rssObject) {
            this.RssObject = rssObject;
        }


    public class Rss {
        Channel ChannelObject;
        private String _version;


        // Getter Methods

        public Channel getChannel() {
            return ChannelObject;
        }



        public String get_version() {
            return _version;
        }

        // Setter Methods

        public void setChannel(Channel channelObject) {
            this.ChannelObject = channelObject;
        }



        public void set_version(String _version) {
            this._version = _version;
        }
    }

    public class Channel {
        private String title;
        private String link;
        private String description;
        private String language;
        private String copyright;
        private String lastBuildDate;
        Image ImageObject;
        private String pubDate;

        public List<Article> getItems() {
            return items;
        }

        public void setItems(List<Article> items) {
            this.items = items;
        }

        private List < Article > items ;


        // Getter Methods

        public String getTitle() {
            return title;
        }

        public String getLink() {
            return link;
        }

        public String getDescription() {
            return description;
        }

        public String getLanguage() {
            return language;
        }

        public String getCopyright() {
            return copyright;
        }

        public String getLastBuildDate() {
            return lastBuildDate;
        }

        public Image getImage() {
            return ImageObject;
        }

        public String getPubDate() {
            return pubDate;
        }



        // Setter Methods

        public void setTitle(String title) {
            this.title = title;
        }

        public void setLink(String link) {
            this.link = link;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public void setCopyright(String copyright) {
            this.copyright = copyright;
        }

        public void setLastBuildDate(String lastBuildDate) {
            this.lastBuildDate = lastBuildDate;
        }

        public void setImage(Image imageObject) {
            this.ImageObject = imageObject;
        }

        public void setPubDate(String pubDate) {
            this.pubDate = pubDate;
        }


    }

    public class Image {
        private String url;
        private String title;
        private String link;


        // Getter Methods

        public String getUrl() {
            return url;
        }

        public String getTitle() {
            return title;
        }

        public String getLink() {
            return link;
        }

        // Setter Methods

        public void setUrl(String url) {
            this.url = url;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public void setLink(String link) {
            this.link = link;
        }
    }

    public class Article{
           private String link , title , description ;

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }
}


