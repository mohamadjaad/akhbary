/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.hashStudio.nabdClone.data.network;

import com.hashStudio.nabdClone.BuildConfig;

/**
 * Created by amitshekhar on 01/02/17.
 */

public final class ApiEndPoint {

    public static final String ENDPOINT_LATEST_NEWS = "https://newsapi.org/v2/top-headlines?country=eg&apiKey=7b9a1e777e1245c5aa2b3c24a1b64223&pagesize=10&page=";

    public static final String ENDPOINT_SPORT = "https://newsapi.org/v2/top-headlines?country=eg&category=sports&apiKey=7b9a1e777e1245c5aa2b3c24a1b64223";

    public static final String ENDPOINT_HEALTH = "https://newsapi.org/v2/top-headlines?country=eg&category=health&apiKey=7b9a1e777e1245c5aa2b3c24a1b64223";

    public static final String ENDPOINT_science= "https://newsapi.org/v2/top-headlines?country=eg&category=science&apiKey=7b9a1e777e1245c5aa2b3c24a1b64223";

    public static final String ENDPOINT_technology= "https://newsapi.org/v2/top-headlines?country=eg&category=technology&apiKey=7b9a1e777e1245c5aa2b3c24a1b64223";

    public static final String ENDPOINT_entertainment= "https://newsapi.org/v2/top-headlines?country=eg&category=entertainment&apiKey=7b9a1e777e1245c5aa2b3c24a1b64223";

    public static final String ENDPOINT_business= "https://newsapi.org/v2/top-headlines?country=eg&category=business&apiKey=7b9a1e777e1245c5aa2b3c24a1b64223";


    public static final String ENDPOINT_ELMASRY_ELYOUM = "https://www.almasryalyoum.com/rss/rssfeeds?homePage=true";

    public static final String ENDPOINT_ELMOGAZ = "http://almogaz.com/rss";

    public static final String ENDPOINT_ELYOUM_ELSABEA = "https://www.youm7.com/rss/SectionRss?SectionID=203";


    public static final String ENDPOINT_search= "https://newsapi.org/v2/everything?q= &apiKey=7b9a1e777e1245c5aa2b3c24a1b64223";


    public static final String ENDPOINT_GOOGLE_LOGIN = BuildConfig.BASE_URL
            + "/588d14f4100000a9072d2943";

    public static final String ENDPOINT_FACEBOOK_LOGIN = BuildConfig.BASE_URL
            + "/588d15d3100000ae072d2944";

    public static final String ENDPOINT_SERVER_LOGIN = BuildConfig.BASE_URL
            + "/588d15f5100000a8072d2945";

    public static final String ENDPOINT_LOGOUT = BuildConfig.BASE_URL
            + "/588d161c100000a9072d2946";

    public static final String ENDPOINT_BLOG = BuildConfig.BASE_URL
            + "/5926ce9d11000096006ccb30";

    public static final String ENDPOINT_OPEN_SOURCE = BuildConfig.BASE_URL
            + "/5926c34212000035026871cd";

    private ApiEndPoint() {
        // This class is not publicly instantiable
    }

}
