/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.hashStudio.nabdClone.data.network;

import com.hashStudio.nabdClone.data.network.model.BlogResponse;
import com.hashStudio.nabdClone.data.network.model.ElmasryAlyoumResponse;
import com.hashStudio.nabdClone.data.network.model.LatestNewsResponse;
import com.hashStudio.nabdClone.data.network.model.LoginRequest;
import com.hashStudio.nabdClone.data.network.model.LoginResponse;
import com.hashStudio.nabdClone.data.network.model.LogoutResponse;
import com.hashStudio.nabdClone.data.network.model.OpenSourceResponse;

import io.reactivex.Single;

public interface ApiHelper {

    ApiHeader getApiHeader();

    Single<LoginResponse> doGoogleLoginApiCall(LoginRequest.GoogleLoginRequest request);

    Single<LoginResponse> doFacebookLoginApiCall(LoginRequest.FacebookLoginRequest request);

    Single<LoginResponse> doServerLoginApiCall(LoginRequest.ServerLoginRequest request);

    Single<LogoutResponse> doLogoutApiCall();

    Single<BlogResponse> getBlogApiCall();

    Single<LatestNewsResponse> getLatestNewsApiCall(int page);

    Single<LatestNewsResponse> getSearchApiCall(String search);

    Single<LatestNewsResponse> getSportApiCall();

    Single<LatestNewsResponse> getScienceApiCall();

    Single<LatestNewsResponse> getEntertainmentApiCall();

    Single<LatestNewsResponse> getHealthApiCall();

    Single<LatestNewsResponse> getTechnologyApiCall();

    Single<OpenSourceResponse> getOpenSourceApiCall();

    Single<ElmasryAlyoumResponse> getElmasryEluoumApiCall();

}
