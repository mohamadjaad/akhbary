package com.hashStudio.nabdClone.data.network.model;

import java.util.List;

public class LatestNewsResponse {

    /**
     * status : ok
     * totalResults : 32
     * articles : [{"source":{"id":null,"name":"Almasryalyoum.com"},"author":null,"title":"حكم نهائي من «الإدارية العليا» بشأن أحقية أصحاب المعاشات في «80% من العلاوات» (فيديو) - Al Masry Al Youm - المصري اليوم","description":"قضت المحكمة الإدارية العليا، برئاسة المستشار أحمد الفقي، نائب رئيس مجلس الدولة، اليوم الخميس، في حكم نهائي وباتّ، برفض الطعون المقامة من وزيرة التضامن الاجتماعي على الحكم الصادر بأحقية أصحاب المعاشات في إضافة 80% من قيمة آخر 5 علاوات إلى الأجر المتغير،","url":"https://www.almasryalyoum.com/news/details/1371994","urlToImage":"https://mediaaws.almasryalyoum.com/news/verylarge/2019/02/21/898940_0.jpg","publishedAt":"2019-02-21T10:19:32Z","content":null},{"source":{"id":null,"name":"Youm7.com"},"author":null,"title":"زوج أنغام الجديد يهنئها بألبومها.. وترد: \"مبروك علينا يا حبيبى\" - اليوم السابع","description":"هنأ زوج الفنانة أنغام الموزع الموسيقى أحمد إبراهيم، زوجته بألبومها الجديد، متمنيا لها النجاح و التوفيق، و تعاون أحمد مع أنغام  فى ألبومها الأخير، والذى حيث وزع لها 6 أغان فى هذا الألبوم.","url":"https://www.youm7.com/story/2019/2/21/%D8%B2%D9%88%D8%AC-%D8%A3%D9%86%D8%BA%D8%A7%D9%85-%D8%A7%D9%84%D8%AC%D8%AF%D9%8A%D8%AF-%D9%8A%D9%87%D9%86%D8%A6%D9%87%D8%A7-%D8%A8%D8%A3%D9%84%D8%A8%D9%88%D9%85%D9%87%D8%A7-%D9%88%D8%AA%D8%B1%D8%AF-%D9%85%D8%A8%D8%B1%D9%88%D9%83-%D8%B9%D9%84%D9%8A%D9%86%D8%A7-%D9%8A%D8%A7-%D8%AD%D8%A8%D9%8A%D8%A8%D9%89/4148372","urlToImage":"https://img.youm7.com/large/201902211118561856.jpg","publishedAt":"2019-02-21T09:32:00Z","content":null},{"source":{"id":null,"name":"Elwatannews.com"},"author":"محمد عيسى","title":"عاجل| «الإدارية العليا» تقضي بأحقية أصحاب المعاشات في العلاوات الـ5 - جريدة الوطن المصرية","description":"قضت المحكمة الإدارية العليا برئاسة المستار أحمد الفقي، نائب رئيس مجلس الدولة، بإلزام مجلس الوزراء بزيادة المعاش","url":"https://www.elwatannews.com/news/details/4012241","urlToImage":"https://watanimg.elwatannews.com/image_archive/648x316/21414697491550741134.jpg","publishedAt":"2019-02-21T09:31:32Z","content":null},{"source":{"id":null,"name":"Masrawy.com"},"author":null,"title":"4 سيناريوهات أمام مائدة مجلس الأهلي لمواجهة اتحاد الكرة - مصراوي","description":"4 سيناريوهات أمام مائدة مجلس الأهلي لمواجهة اتحاد الكرة...مصراوى","url":"https://www.masrawy.com/sports/sports_news/details/2019/2/21/1518570/4-%D8%B3%D9%8A%D9%86%D8%A7%D8%B1%D9%8A%D9%88%D9%87%D8%A7%D8%AA-%D8%A3%D9%85%D8%A7%D9%85-%D9%85%D8%A7%D8%A6%D8%AF%D8%A9-%D9%85%D8%AC%D9%84%D8%B3-%D8%A7%D9%84%D8%A3%D9%87%D9%84%D9%8A-%D9%84%D9%85%D9%88%D8%A7%D8%AC%D9%87%D8%A9-%D8%A7%D8%AA%D8%AD%D8%A7%D8%AF-%D8%A7%D9%84%D9%83%D8%B1%D8%A9","urlToImage":"https://media.linkonlineworld.com/img/large/2017/12/10/2017_12_10_13_42_57_264.jpg","publishedAt":"2019-02-21T09:31:00Z","content":"- :. 28 18. 28. \"\" 4 : 1- \" \".. 2- \" \". 3- 1985 1997. 2014. 4-. (-). 4.. 30..... 28.. ( ).."},{"source":{"id":null,"name":"Masrawy.com"},"author":null,"title":"أسعار الدولار تنخفض أمام الجنيه في 6 بنوك مع بداية التعاملات - مصراوي","description":"أسعار الدولار تنخفض أمام الجنيه في 6 بنوك مع بداية التعاملات...مصراوى","url":"https://www.masrawy.com/news/news_economy/details/2019/2/21/1518519/-%D8%A3%D8%B3%D8%B9%D8%A7%D8%B1-%D8%A7%D9%84%D8%AF%D9%88%D9%84%D8%A7%D8%B1-%D8%AA%D9%86%D8%AE%D9%81%D8%B6-%D8%A3%D9%85%D8%A7%D9%85-%D8%A7%D9%84%D8%AC%D9%86%D9%8A%D9%87-%D9%81%D9%8A-6-%D8%A8%D9%86%D9%88%D9%83-%D9%85%D8%B9-%D8%A8%D8%AF%D8%A7%D9%8A%D8%A9-%D8%A7%D9%84%D8%AA%D8%B9%D8%A7%D9%85%D9%84%D8%A7%D8%AA","urlToImage":"https://media.linkonlineworld.com/img/large/2019/2/13/2019_2_13_17_10_6_800.jpg","publishedAt":"2019-02-21T08:38:00Z","content":"- : 4 6 5. 4 17.49 17.59. 17.47 17.57. 17.51 17.61. 17.52 17.62. 17.49 17.59. 17.52 17.62. 17.53 17.63. 17.54 17.63."},{"source":{"id":null,"name":"Masrawy.com"},"author":null,"title":"عبد الحفيظ: التجديد لأزارو؟ عقده ممتد 3 سنوات.. ولا أتمنى عودة مؤمن - مصراوي","description":"عبد الحفيظ التجديد لأزارو عقده ممتد 3 سنوات ولا أتمنى عودة مؤمن...مصراوى","url":"https://www.masrawy.com/sports/sports_news/details/2019/2/21/1518518/%D8%B9%D8%A8%D8%AF-%D8%A7%D9%84%D8%AD%D9%81%D9%8A%D8%B8-%D8%A7%D9%84%D8%AA%D8%AC%D8%AF%D9%8A%D8%AF-%D9%84%D8%A3%D8%B2%D8%A7%D8%B1%D9%88-%D8%B9%D9%82%D8%AF%D9%87-%D9%85%D9%85%D8%AA%D8%AF-3-%D8%B3%D9%86%D9%88%D8%A7%D8%AA-%D9%88%D9%84%D8%A7-%D8%A3%D8%AA%D9%85%D9%86%D9%89-%D8%B9%D9%88%D8%AF%D8%A9-%D9%85%D8%A4%D9%85%D9%86","urlToImage":"https://media.linkonlineworld.com/img/large/2018/11/6/2018_11_6_6_36_26_801.jpg","publishedAt":"2019-02-21T08:34:00Z","content":"- :. \" \" :\" \". :\" \".. :\" \". :\" \".."},{"source":{"id":null,"name":"Yallakora.com"},"author":"محمد همام","title":"بيراميدز: نرفض إجراء تعديلات في جدول الدوري والكأس إرضاءً لأطراف أخرى - يلا كورة","description":"أصدر نادي بيراميدز صباح اليوم الخميس، بيانًا رسميًا يؤكد من خلاله رفضه القاطع إجراء أي تعديلات جديدة على مسابقتي الدوري والكأس.","url":"https://www.yallakora.com/egyptian-league/2544/News/361666/%D8%A8%D9%8A%D8%B1%D8%A7%D9%85%D9%8A%D8%AF%D8%B2-%D9%86%D8%B1%D9%81%D8%B6-%D8%A5%D8%AC%D8%B1%D8%A7%D8%A1-%D8%AA%D8%B9%D8%AF%D9%8A%D9%84%D8%A7%D8%AA-%D9%81%D9%8A-%D8%AC%D8%AF%D9%88%D9%84-%D8%A7%D9%84%D8%AF%D9%88%D8%B1%D9%8A-%D9%88%D8%A7%D9%84%D9%83%D8%A3%D8%B3-%D8%A5%D8%B1%D8%B6%D8%A7%D8%A1-%D9%84%D8%A3%D8%B7%D8%B1%D8%A7%D9%81-%D8%A3%D8%AE%D8%B1%D9%89","urlToImage":"https://media.linkonlineworld.com/img/yallakora/Normal/\\2019\\2\\14\\52340172-2255588614504543-2024044432487088128-n2019_2_14_21_55.jpg","publishedAt":"2019-02-21T08:23:00Z","content":": :.. :\" \". :\" \".. 28 16....... : : 3.."},{"source":{"id":null,"name":"Youm7.com"},"author":null,"title":"الدولار يصعد بعد محضر المركزى الأمريكى والأسترالى يتراجع - اليوم السابع","description":"ارتفع الدولار، اليوم الخميس، بعد أن أنعش محضر أحدث اجتماع لمجلس الاحتياطى الاتحادى (البنك المركزى الأمريكى) توقعات زيادة محتملة لأسعار الفائدة.","url":"https://www.youm7.com/story/2019/2/21/%D8%A7%D9%84%D8%AF%D9%88%D9%84%D8%A7%D8%B1-%D9%8A%D8%B5%D8%B9%D8%AF-%D8%A8%D8%B9%D8%AF-%D9%85%D8%AD%D8%B6%D8%B1-%D8%A7%D9%84%D9%85%D8%B1%D9%83%D8%B2%D9%89-%D8%A7%D9%84%D8%A3%D9%85%D8%B1%D9%8A%D9%83%D9%89-%D9%88%D8%A7%D9%84%D8%A3%D8%B3%D8%AA%D8%B1%D8%A7%D9%84%D9%89-%D9%8A%D8%AA%D8%B1%D8%A7%D8%AC%D8%B9/4148245","urlToImage":"https://img.youm7.com/large/201811240958345834.jpg","publishedAt":"2019-02-21T07:34:00Z","content":null},{"source":{"id":"rt","name":"RT"},"author":null,"title":"دراسة تكشف متى يؤدي الزواج إلى الموت! - روسيا اليوم","description":"وجدت دراسة حديثة أن العيش مع شركاء لا يتمتعون بالقدرة على الاستماع إلى المشاكل، يؤثر سلبا على صحة الناس على المدى الطويل.","url":"https://arabic.rt.com/health/1002571-%D8%AF%D8%B1%D8%A7%D8%B3%D8%A9-%D8%B4%D8%B1%D9%8A%D9%83-%D8%A7%D9%84%D8%AD%D9%8A%D8%A7%D8%A9-%D9%88%D9%81%D8%A7%D8%AA%D9%83/","urlToImage":"https://cdni.rt.com/media/pics/2019.02/original/5c6d62bdd437500a7e8b4587.jpg","publishedAt":"2019-02-21T05:00:00Z","content":null},{"source":{"id":null,"name":"Akhbarak.net"},"author":"akhbarak","title":"Galaxy S10 Plus جالاكسي اس 10 بلس المواصفات والمميزات والسعر - صدى التقنية - تكنولوجيا - أخبارك دوت نت","description":"Galaxy S10 Plus جالاكسي اس 10 بلس: المواصفات والمميزات والسعر - صدى التقنية","url":"http://www.akhbarak.net/articles/35981669-%D8%A7%D9%84%D9%85%D9%82%D8%A7%D9%84-%D9%85%D9%86-%D8%A7%D9%84%D9%85%D8%B5%D8%AF%D8%B1-Galaxy-S10-Plus-%D8%AC%D8%A7%D9%84%D8%A7%D9%83%D8%B3%D9%8A-%D8%A7%D8%B3","urlToImage":"http://cdn.akhbarak.net/photos/articles-photos/2019/2/21/35981669/35981669-og.jpg?1550720730","publishedAt":"2019-02-21T04:11:29Z","content":null},{"source":{"id":null,"name":"Elbalad.news"},"author":"https://www.facebook.com/ElBaladOfficial","title":"والد إرهابي يعترف بأن نجله أحد جنود الجماعات الإرهابية .. فيديو - Sada El-Bald صدى البلد","description":"قام والد أحد الارهابيين المنفذ فيهم حكم الإعدام فى قضية اغتيال النائب العام هشام بركات، بقراءة وصية الإرهابى التى يعترف فيها بأنه أحد جنود الجماعات الإرهابية وأنه اختار طريقه فى ذلك. وقال عبد المجيد، ...","url":"https://www.elbalad.news/3710629","urlToImage":"https://www.elbalad.news/upload/photo/news/371/0/560x292o/629.jpg","publishedAt":"2019-02-20T22:37:00Z","content":null},{"source":{"id":null,"name":"Youm7.com"},"author":null,"title":"حظك اليوم وتوقعات الأبراج الخميس 21/2/2019 على الصعيد المهنى العاطفى والصحى - اليوم السابع","description":"هناك الكثيرون لا يخطون خطوة واحدة دون متابعة الأبراج والفلك، ومعرفة يوم حظهم، لذلك نقدم لكم خدمة حظك اليوم لكل الأبراج،","url":"https://www.youm7.com/story/2019/2/21/%D8%AD%D8%B8%D9%83-%D8%A7%D9%84%D9%8A%D9%88%D9%85-%D9%88%D8%AA%D9%88%D9%82%D8%B9%D8%A7%D8%AA-%D8%A7%D9%84%D8%A3%D8%A8%D8%B1%D8%A7%D8%AC-%D8%A7%D9%84%D8%AE%D9%85%D9%8A%D8%B3-21-2-2019-%D8%B9%D9%84%D9%89-%D8%A7%D9%84%D8%B5%D8%B9%D9%8A%D8%AF/4147059","urlToImage":"https://img.youm7.com/large/201811120148474847.jpg","publishedAt":"2019-02-20T22:00:00Z","content":". 21/2/2019.. \" \"..... 21/2/2019.. \" \"..... 21/2/2019.. \" \"..... 21/2/2019.. \" \"..... 21/2/2019.. \" \"..... 21/2/2019.. \" \".... 21/2/2019.. \" \"..... 21/2/2019.. \" \"..... 21/2/2019.. \" \"..... 21/2/2019.. 21/2/2019.. \" \".... 21/21/2019.. \" \"....."},{"source":{"id":null,"name":"Bbc.com"},"author":"https://www.facebook.com/bbcnews","title":"بسبب \"التحرش\" ...مطالبات بمعاقبة أحد نجوم \"سناب شات\" في السعودية - BBC Arabic","description":"انتشر مؤخرا في السعودية حملة هاشتاغ \"نطالب بمعاقبة المتحرش بن كرمان\" فما القصة ومن هو بن كرمان؟","url":"http://www.bbc.com/arabic/trending-47305177","urlToImage":"https://ichef.bbci.co.uk/news/1024/branded_arabic/6D08/production/_105721972_mediaitem105721969.jpg","publishedAt":"2019-02-20T21:51:45Z","content":null},{"source":{"id":"rt","name":"RT"},"author":null,"title":"ست علامات تدل على ضعف عمل الغدة الدرقية - روسيا اليوم","description":"تقع الغدة الدرقية وهي أبرز الغدد الصماء في جسم الإنسان، أمام القصبة الهوائية، حيث تفرز هذه الغدة هرمون الثايرويد بنوعيه، الثيروكسين (T4) وثالث يود الثيرونين (T3).","url":"https://arabic.rt.com/health/1002171-%D8%B3%D8%AA-%D8%B9%D9%84%D8%A7%D9%85%D8%A7%D8%AA-%D8%AA%D8%AF%D9%84-%D8%B9%D9%84%D9%89-%D8%B6%D8%B9%D9%81-%D8%B9%D9%85%D9%84-%D8%A7%D9%84%D8%BA%D8%AF%D8%A9-%D8%A7%D9%84%D8%AF%D8%B1%D9%82%D9%8A%D8%A9/","urlToImage":"https://cdni.rt.com/media/pics/2019.02/original/5c63f79695a597da0e8b45f8.JPG","publishedAt":"2019-02-20T21:00:00Z","content":null},{"source":{"id":null,"name":"Skynewsarabia.com"},"author":"سكاي نيوز عربية","title":"مطورو \"فورتنايت\" يقاضون مهرجانا \"استخف بلعبتهم\" - Sky News Arabia سكاي نيوز عربية","description":"قررت شركة \"إيبك غيمز\" مقاضاة القائمين على مهرجان \"فورتنايت فورتنايتلايف\"، الذي نظم في بريطانيا بعطلة نهاية الأسبوع الماضية، وفق ما ذكر موقع \"تك سبوت\".","url":"https://www.skynewsarabia.com/varieties/1229117-%D9%85%D8%B7%D9%88%D8%B1%D9%88-%D9%81%D9%88%D8%B1%D8%AA%D9%86%D8%A7%D9%8A%D8%AA-%D9%8A%D9%82%D8%A7%D8%B6%D9%88%D9%86-%D9%85%D9%87%D8%B1%D8%AC%D8%A7%D9%86%D8%A7-%D8%A7%D8%B3%D8%AA%D8%AE%D9%81-%D8%A8%D9%84%D8%B9%D8%A8%D8%AA%D9%87%D9%85","urlToImage":"https://www.skynewsarabia.com/images/v1/2019/02/20/1229114/1200/630/1-1229114.jpg","publishedAt":"2019-02-20T20:57:56Z","content":null},{"source":{"id":null,"name":"Masrawy.com"},"author":null,"title":"قوة عسكرية قطرية تصل السعودية لبدء تمرين مشترك - مصراوي","description":"قوة عسكرية قطرية تصل السعودية لبدء تمرين مشترك...مصراوى","url":"https://www.masrawy.com/news/news_publicaffairs/details/2019/2/20/1518417/%D9%82%D9%88%D8%A9-%D8%B9%D8%B3%D9%83%D8%B1%D9%8A%D8%A9-%D9%82%D8%B7%D8%B1%D9%8A%D8%A9-%D8%AA%D8%B5%D9%84-%D8%A7%D9%84%D8%B3%D8%B9%D9%88%D8%AF%D9%8A%D8%A9-%D9%84%D8%A8%D8%AF%D8%A1-%D8%AA%D9%85%D8%B1%D9%8A%D9%86-%D9%85%D8%B4%D8%AA%D8%B1%D9%83","urlToImage":"https://media.linkonlineworld.com/img/large/2019/2/20/2019_2_20_22_55_38_44.jpg","publishedAt":"2019-02-20T20:55:00Z","content":""},{"source":{"id":null,"name":"Masrawy.com"},"author":null,"title":"بـ 3 نسخ جديدة.. سامسونج تكشف عن هاتف \"جالكسي إس 10\" (صور) - مصراوي","description":"بـ 3 نسخ جديدة سامسونج تكشف عن هاتف جالكسي إس 10 صور...مصراوى","url":"https://www.masrawy.com/news/tech-reports/details/2019/2/20/1518413/%D8%A8%D9%80-3-%D9%86%D8%B3%D8%AE-%D8%AC%D8%AF%D9%8A%D8%AF%D8%A9-%D8%B3%D8%A7%D9%85%D8%B3%D9%88%D9%86%D8%AC-%D8%AA%D9%83%D8%B4%D9%81-%D8%B9%D9%86-%D9%87%D8%A7%D8%AA%D9%81-%D8%AC%D8%A7%D9%84%D9%83%D8%B3%D9%8A-%D8%A5%D8%B3-10-%D8%B5%D9%88%D8%B1-","urlToImage":"https://media.linkonlineworld.com/img/large/2019/2/20/2019_2_20_22_47_24_554.jpg","publishedAt":"2019-02-20T20:48:00Z","content":"- : \" \" \" 10\" \" 10 \" \" 10 \" \"Samsung Galaxy UNPACKED 2019\". 3. : 1- \" 10\" \" \" \"6.1 \" QHD+ HDR10. \" 10\" \" 9.0\" One UI \" 855\" \"6 \" \"128 \". \"12 - 12 - 16 \" \"8 - 10 \". \" 10\" \"3400\" /. \" 10\" 900. 2- \" 10 \" \" \" \"6.4 \" QHD+ HDR10. \" 10 \" \" 9.0\" One UI \" 855\" \"6 - 8 \u2026 [+211 chars]"},{"source":{"id":null,"name":"Masrawy.com"},"author":null,"title":"بـ 6 كاميرات.. سامسونج تكشف عن هاتفها القابل للطي \"جالكسي فولد\" (صور) - مصراوي","description":"بـ 6 كاميرات سامسونج تكشف عن هاتفها القابل للطي جالكسي فولد صور...مصراوى","url":"https://www.masrawy.com/news/tech-reports/details/2019/2/20/1518381/%D8%A8%D9%80-6-%D9%83%D8%A7%D9%85%D9%8A%D8%B1%D8%A7%D8%AA-%D8%B3%D8%A7%D9%85%D8%B3%D9%88%D9%86%D8%AC-%D8%AA%D9%83%D8%B4%D9%81-%D8%B9%D9%86-%D9%87%D8%A7%D8%AA%D9%81%D9%87%D8%A7-%D8%A7%D9%84%D9%82%D8%A7%D8%A8%D9%84-%D9%84%D9%84%D8%B7%D9%8A-%D8%AC%D8%A7%D9%84%D9%83%D8%B3%D9%8A-%D9%81%D9%88%D9%84%D8%AF-%D8%B5%D9%88%D8%B1-","urlToImage":"https://media.linkonlineworld.com/img/large/2019/2/20/2019_2_20_21_49_13_798.png","publishedAt":"2019-02-20T19:49:00Z","content":"- : \" \" \"Samsung Galaxy UNPACKED 2019\". \"4.3 \" \"7.3 \".. \"\". \" \" \"12 \" \"512 \". 6. \" \" 4380 /. \"\" 26 1980."},{"source":{"id":null,"name":"Youm7.com"},"author":null,"title":"بعد ظهوره فى 24 ولاية أمريكية..مخاوف من انتقال مرض \"زومبى الغزلان\" للبشر - اليوم السابع","description":"حالة من القلق والذعر أصابت العلماء والمسئولين فى الولايات المتحدة الأمريكية بعد انتشار مرض \"زومبى الغزلان\" أو ما يطلق عليه فى الأصل \"الهزال المزمن CWD\"","url":"https://www.youm7.com/story/2019/2/20/%D8%A8%D8%B9%D8%AF-%D8%B8%D9%87%D9%88%D8%B1%D9%87-%D9%81%D9%89-24-%D9%88%D9%84%D8%A7%D9%8A%D8%A9-%D8%A3%D9%85%D8%B1%D9%8A%D9%83%D9%8A%D8%A9-%D9%85%D8%AE%D8%A7%D9%88%D9%81-%D9%85%D9%86-%D8%A7%D9%86%D8%AA%D9%82%D8%A7%D9%84-%D9%85%D8%B1%D8%B6/4147626","urlToImage":"https://img.youm7.com/large/201902200355105510.jpg","publishedAt":"2019-02-20T18:46:00Z","content":null},{"source":{"id":null,"name":"Yallakora.com"},"author":"هادي المدني","title":"الدوري الممتاز.. الأهلي 0-0 الداخلية.. بداية المباراة - يلا كورة","description":"فاز الأهلي على ضيفه الداخلية بنتيجة 3-1 في المباراة التي جمعت الفريقين اليوم الأربعاء ضمن الجولة الـ23 من الدوري الممتاز على استاد بترو سبورت.","url":"https://www.yallakora.com/egyptian-league/2544/News/361650/%D8%A8%D8%B9%D8%AF-%D9%82%D9%84%D9%8A%D9%84-%D8%A7%D9%84%D8%A3%D9%87%D9%84%D9%8A-%D9%88%D8%A7%D9%84%D8%AF%D8%A7%D8%AE%D9%84%D9%8A%D8%A9-%D8%A7%D9%84%D8%AF%D9%88%D8%B1%D9%8A-%D8%A7%D9%84%D9%85%D9%85%D8%AA%D8%A7%D8%B2","urlToImage":"https://media.linkonlineworld.com/img/yallakora/Normal/\\2019\\2\\20\\52474338-2265003066896431-392286747855159296-n2019_2_20_21_30.jpg","publishedAt":"2019-02-20T17:23:00Z","content":": : 3-1 23. 45 7 22 14.. 7. 20. 29. 38. 39.. 52.. 60. 66. 71. 72. 73. \"\" 78. 83. 83. 85. 90."}]
     */

    private String status;
    private int totalResults;
    private List<ArticlesBean> articles;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(int totalResults) {
        this.totalResults = totalResults;
    }

    public List<ArticlesBean> getArticles() {
        return articles;
    }

    public void setArticles(List<ArticlesBean> articles) {
        this.articles = articles;
    }

    public static class ArticlesBean {
        /**
         * source : {"id":null,"name":"Almasryalyoum.com"}
         * author : null
         * title : حكم نهائي من «الإدارية العليا» بشأن أحقية أصحاب المعاشات في «80% من العلاوات» (فيديو) - Al Masry Al Youm - المصري اليوم
         * description : قضت المحكمة الإدارية العليا، برئاسة المستشار أحمد الفقي، نائب رئيس مجلس الدولة، اليوم الخميس، في حكم نهائي وباتّ، برفض الطعون المقامة من وزيرة التضامن الاجتماعي على الحكم الصادر بأحقية أصحاب المعاشات في إضافة 80% من قيمة آخر 5 علاوات إلى الأجر المتغير،
         * url : https://www.almasryalyoum.com/news/details/1371994
         * urlToImage : https://mediaaws.almasryalyoum.com/news/verylarge/2019/02/21/898940_0.jpg
         * publishedAt : 2019-02-21T10:19:32Z
         * content : null
         */

        private SourceBean source;
        private Object author;
        private String title;
        private String description;
        private String url;
        private String urlToImage;
        private String publishedAt;
        private Object content;

        public SourceBean getSource() {
            return source;
        }

        public void setSource(SourceBean source) {
            this.source = source;
        }

        public Object getAuthor() {
            return author;
        }

        public void setAuthor(Object author) {
            this.author = author;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getUrlToImage() {
            return urlToImage;
        }

        public void setUrlToImage(String urlToImage) {
            this.urlToImage = urlToImage;
        }

        public String getPublishedAt() {
            return publishedAt;
        }

        public void setPublishedAt(String publishedAt) {
            this.publishedAt = publishedAt;
        }

        public Object getContent() {
            return content;
        }

        public void setContent(Object content) {
            this.content = content;
        }

        public static class SourceBean {
            /**
             * id : null
             * name : Almasryalyoum.com
             */

            private Object id;
            private String name;

            public Object getId() {
                return id;
            }

            public void setId(Object id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }
    }
}
