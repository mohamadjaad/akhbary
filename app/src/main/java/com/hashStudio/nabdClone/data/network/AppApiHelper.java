/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.hashStudio.nabdClone.data.network;

import android.content.Context;
import android.net.ConnectivityManager;

import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.hashStudio.nabdClone.data.network.model.BlogResponse;
import com.hashStudio.nabdClone.data.network.model.ElmasryAlyoumResponse;
import com.hashStudio.nabdClone.data.network.model.LatestNewsResponse;
import com.hashStudio.nabdClone.data.network.model.LoginRequest;
import com.hashStudio.nabdClone.data.network.model.LoginResponse;
import com.hashStudio.nabdClone.data.network.model.LogoutResponse;
import com.hashStudio.nabdClone.data.network.model.OpenSourceResponse;
import com.rx2androidnetworking.Rx2AndroidNetworking;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Single;

/**
 * Created by janisharali on 28/01/17.
 */

@Singleton
public class AppApiHelper implements ApiHelper {

    private ApiHeader mApiHeader;

    @Inject
    public AppApiHelper(ApiHeader apiHeader) {
        mApiHeader = apiHeader;
    }

    @Override
    public ApiHeader getApiHeader() {
        return mApiHeader;
    }

    @Override
    public Single<LoginResponse> doGoogleLoginApiCall(LoginRequest.GoogleLoginRequest
                                                              request) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_GOOGLE_LOGIN)
                .addHeaders(mApiHeader.getPublicApiHeader())
                .addBodyParameter(request)
                .build()
                .getObjectSingle(LoginResponse.class);
    }

    @Override
    public Single<LoginResponse> doFacebookLoginApiCall(LoginRequest.FacebookLoginRequest
                                                                request) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_FACEBOOK_LOGIN)
                .addHeaders(mApiHeader.getPublicApiHeader())
                .addBodyParameter(request)
                .build()
                .getObjectSingle(LoginResponse.class);
    }

    @Override
    public Single<LoginResponse> doServerLoginApiCall(LoginRequest.ServerLoginRequest
                                                              request) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SERVER_LOGIN)
                .addHeaders(mApiHeader.getPublicApiHeader())
                .addBodyParameter(request)
                .build()
                .getObjectSingle(LoginResponse.class);
    }

    @Override
    public Single<LogoutResponse> doLogoutApiCall() {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_LOGOUT)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .build()
                .getObjectSingle(LogoutResponse.class);
    }

    @Override
    public Single<BlogResponse> getBlogApiCall() {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_BLOG)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .build()
                .getObjectSingle(BlogResponse.class);
    }

    @Override
    public Single<LatestNewsResponse> getLatestNewsApiCall(int page) {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_LATEST_NEWS + page )
                .build()
                .getObjectSingle(LatestNewsResponse.class);
    }

    @Override
    public Single<LatestNewsResponse> getSportApiCall() {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_SPORT)
                .build()
                .getObjectSingle(LatestNewsResponse.class);
    }

    @Override
    public Single<LatestNewsResponse> getScienceApiCall() {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_science)
                .build()
                .getObjectSingle(LatestNewsResponse.class);
    }

    @Override
    public Single<LatestNewsResponse> getEntertainmentApiCall() {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_entertainment)
                .build()
                .getObjectSingle(LatestNewsResponse.class);
    }

    @Override
    public Single<LatestNewsResponse> getHealthApiCall() {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_HEALTH)
                .build()
                .getObjectSingle(LatestNewsResponse.class);
    }

    @Override
    public Single<LatestNewsResponse> getTechnologyApiCall() {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_technology)
                .build()
                .getObjectSingle(LatestNewsResponse.class);
    }

    @Override
    public Single<OpenSourceResponse> getOpenSourceApiCall() {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_OPEN_SOURCE)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .build()
                .getObjectSingle(OpenSourceResponse.class);
    }

    @Override
    public Single<ElmasryAlyoumResponse> getElmasryEluoumApiCall() {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_ELMASRY_ELYOUM)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .build()
                .getObjectSingle(ElmasryAlyoumResponse.class);
    }

    @Override
    public Single<LatestNewsResponse> getSearchApiCall(String search) {
        return Rx2AndroidNetworking.get("https://newsapi.org/v2/everything?q="
                + search
                + "&apiKey=7b9a1e777e1245c5aa2b3c24a1b64223")
                .build()
                .getObjectSingle(LatestNewsResponse.class);
    }


}

