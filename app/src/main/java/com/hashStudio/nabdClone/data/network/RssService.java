package com.hashStudio.nabdClone.data.network;

import me.toptas.rssconverter.RssFeed;
import retrofit2.Call;
import retrofit2.http.GET;


public interface RssService {


    //aawsat.com/feed
    //http://www.bbc.co.uk/arabic/index.xml
    //http://arabic.cnn.com/rss/cnnarabic_topnews.rss
    //http://rss.dw.de/rdf/rss-ar-news
    //https://www.alarabiya.net/.mrss/ar.xml
    //https://www.almasryalyoum.com/rss/rssfeeds?homePage=true
    //https://www.youm7.com/rss/SectionRss?SectionID=203
    //http://almogaz.com/rss
    //https://www.egypt-today.com/home/rss.xml
    //https://misr5.com/app_rss.php?cat=25292
    //https://www.shorouknews.com/egypt/rss
    //https://www.arabstoday.net/home/rss.xml

    @GET("https://www.arabstoday.net/home/rss.xml")
    Call<RssFeed> getRss();

    @GET("https://www.youm7.com/rss/SectionRss?SectionID=203")
    Call<RssFeed> getRssYoumSabea();

    @GET("https://www.egypt-today.com/home/rss.xml")
    Call<RssFeed> getRssEgyToday();

    @GET("https://www.masrawy.com/rssfeed/35/%D9%85%D8%B5%D8%B1")
    Call<RssFeed> getRssMasrawy();

    @GET("https://www.alarabiya.net/.mrss/ar.xml")
    Call<RssFeed> getRssArabyanet();

    @GET("http://almogaz.com/rss")
    Call<RssFeed> getRssMogaz();

    @GET("https://www.almasryalyoum.com/rss/rssfeeds?homePage=true")
    Call<RssFeed> getElmasryElyoum();
}
