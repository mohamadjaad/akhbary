/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.hashStudio.nabdClone.di.module;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;

import com.hashStudio.nabdClone.data.network.model.BlogResponse;
import com.hashStudio.nabdClone.data.network.model.LatestNewsResponse;
import com.hashStudio.nabdClone.data.network.model.OpenSourceResponse;
import com.hashStudio.nabdClone.di.ActivityContext;
import com.hashStudio.nabdClone.di.PerActivity;
import com.hashStudio.nabdClone.ui.Category.CategoryAdapter;
import com.hashStudio.nabdClone.ui.Category.CategoryMvpPresenter;
import com.hashStudio.nabdClone.ui.Category.CategoryMvpView;
import com.hashStudio.nabdClone.ui.Category.CategoryPresenter;
import com.hashStudio.nabdClone.ui.Magazine.MagazineMvpPresenter;
import com.hashStudio.nabdClone.ui.Magazine.MagazineMvpView;
import com.hashStudio.nabdClone.ui.Magazine.MagazinePresenter;
import com.hashStudio.nabdClone.ui.feed.FeedMvpPresenter;
import com.hashStudio.nabdClone.ui.feed.FeedMvpView;
import com.hashStudio.nabdClone.ui.feed.FeedPagerAdapter;
import com.hashStudio.nabdClone.ui.feed.FeedPresenter;
import com.hashStudio.nabdClone.ui.feed.mostRead.MostReadAdapter;
import com.hashStudio.nabdClone.ui.feed.mostRead.MostReadMvpPresenter;
import com.hashStudio.nabdClone.ui.feed.mostRead.MostReadMvpView;
import com.hashStudio.nabdClone.ui.feed.mostRead.MostReadPresenter;
import com.hashStudio.nabdClone.ui.latestNews.LatestNewsAdapter;
import com.hashStudio.nabdClone.ui.latestNews.LatestNewsMvpPresenter;
import com.hashStudio.nabdClone.ui.latestNews.LatestNewsMvpView;
import com.hashStudio.nabdClone.ui.latestNews.LatestNewsPresenter;
import com.hashStudio.nabdClone.ui.liveChannels.LiveChannelsAdapter;
import com.hashStudio.nabdClone.ui.liveChannels.LiveChannelsMvpPresenter;
import com.hashStudio.nabdClone.ui.liveChannels.LiveChannelsMvpView;
import com.hashStudio.nabdClone.ui.liveChannels.LiveChannelsPresenter;
import com.hashStudio.nabdClone.ui.login.LoginMvpPresenter;
import com.hashStudio.nabdClone.ui.login.LoginMvpView;
import com.hashStudio.nabdClone.ui.login.LoginPresenter;
import com.hashStudio.nabdClone.ui.main2.Main2MvpPresenter;
import com.hashStudio.nabdClone.ui.main2.Main2MvpView;
import com.hashStudio.nabdClone.ui.main2.Main2Presenter;
import com.hashStudio.nabdClone.ui.myAccount.MyAccountMvpPresenter;
import com.hashStudio.nabdClone.ui.myAccount.MyAccountMvpView;
import com.hashStudio.nabdClone.ui.myAccount.MyAccountPresenter;
import com.hashStudio.nabdClone.ui.sources.SourceAdapter;
import com.hashStudio.nabdClone.ui.sources.SourceMvpPresenter;
import com.hashStudio.nabdClone.ui.sources.SourceMvpView;
import com.hashStudio.nabdClone.ui.sources.SourcePresenter;
import com.hashStudio.nabdClone.ui.splash.SplashMvpPresenter;
import com.hashStudio.nabdClone.ui.splash.SplashMvpView;
import com.hashStudio.nabdClone.ui.splash.SplashPresenter;
import com.hashStudio.nabdClone.ui.sports.SportMvpPresenter;
import com.hashStudio.nabdClone.ui.sports.SportMvpView;
import com.hashStudio.nabdClone.ui.sports.SportPresenter;
import com.hashStudio.nabdClone.ui.sports.SportsAdapter;
import com.hashStudio.nabdClone.utils.rx.AppSchedulerProvider;
import com.hashStudio.nabdClone.utils.rx.SchedulerProvider;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;
import me.toptas.rssconverter.RssItem;

@Module
public class ActivityModule {

    private AppCompatActivity mActivity;

    public ActivityModule(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    @Provides
    @ActivityContext
    Context provideContext() {
        return mActivity;
    }

    @Provides
    AppCompatActivity provideActivity() {
        return mActivity;
    }

    @Provides
    CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Provides
    SchedulerProvider provideSchedulerProvider() {
        return new AppSchedulerProvider();
    }

    @Provides
    @PerActivity
    SplashMvpPresenter<SplashMvpView> provideSplashPresenter(
            SplashPresenter<SplashMvpView> presenter) {
        return presenter;
    }

    @Provides
    Main2MvpPresenter<Main2MvpView> provideMain2Presenter(
            Main2Presenter<Main2MvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    LoginMvpPresenter<LoginMvpView> provideLoginPresenter(
            LoginPresenter<LoginMvpView> presenter) {
        return presenter;
    }

    @Provides
    FeedMvpPresenter<FeedMvpView> provideFeedPresenter(
            FeedPresenter<FeedMvpView> presenter) {
        return presenter;
    }

    @Provides
    MostReadMvpPresenter<MostReadMvpView> provideMostReadPresenter(
            MostReadPresenter<MostReadMvpView> presenter) {
        return presenter;
    }

    @Provides
    SportMvpPresenter<SportMvpView> provideSportPresenter(
            SportPresenter<SportMvpView> presenter) {
        return presenter;
    }

    @Provides
    CategoryMvpPresenter<CategoryMvpView> provideCategoryPresenter(
            CategoryPresenter<CategoryMvpView> presenter) {
        return presenter;
    }

    @Provides
    MyAccountMvpPresenter<MyAccountMvpView> provideMyAccountPresenter(
            MyAccountPresenter<MyAccountMvpView> presenter) {
        return presenter;
    }

    @Provides
    LiveChannelsMvpPresenter<LiveChannelsMvpView> provideMyResourcesPresenter(
            LiveChannelsPresenter<LiveChannelsMvpView> presenter) {
        return presenter;
    }

    @Provides
    SourceMvpPresenter<SourceMvpView> provideUrgentMvpPresenter(
            SourcePresenter<SourceMvpView> presenter) {
        return presenter;
    }

    @Provides
    LatestNewsMvpPresenter<LatestNewsMvpView> provideLatestNewsMvpPresenter(
            LatestNewsPresenter<LatestNewsMvpView> presenter) {
        return presenter;
    }

    @Provides
    MagazineMvpPresenter<MagazineMvpView> provideMagazineMvpPresenter(
            MagazinePresenter<MagazineMvpView> presenter) {
        return presenter;
    }

    @Provides
    FeedPagerAdapter provideFeedPagerAdapter(AppCompatActivity activity) {
        return new FeedPagerAdapter(activity.getSupportFragmentManager());
    }

    @Provides
    SportsAdapter provideSportsAdapter(AppCompatActivity activity) {
        return new SportsAdapter(new ArrayList<LatestNewsResponse.ArticlesBean>());
    }

    @Provides
    CategoryAdapter provideCategoryAdapter(AppCompatActivity activity) {
        return new CategoryAdapter(new ArrayList<LatestNewsResponse.ArticlesBean>());
    }

    @Provides
    MostReadAdapter provideOpenSourceAdapter() {
        return new MostReadAdapter(new ArrayList<OpenSourceResponse.Repo>());
    }


    @Provides
    LiveChannelsAdapter provideMyResourcesAdapter() {
        return new LiveChannelsAdapter(new ArrayList<BlogResponse.Blog>());
    }

    @Provides
    LatestNewsAdapter provideLatestNewsAdapter() {
        return new LatestNewsAdapter(new ArrayList<LatestNewsResponse.ArticlesBean>());
    }

    @Provides
    SourceAdapter provideSourceAdapter() {
        return new SourceAdapter(new ArrayList<RssItem>());
    }

    @Provides
    LinearLayoutManager provideLinearLayoutManager(AppCompatActivity activity) {
        return new LinearLayoutManager(activity);
    }
}
