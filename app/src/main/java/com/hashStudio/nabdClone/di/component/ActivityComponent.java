/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.hashStudio.nabdClone.di.component;

import com.hashStudio.nabdClone.di.PerActivity;
import com.hashStudio.nabdClone.di.module.ActivityModule;
import com.hashStudio.nabdClone.ui.Category.CategoryActivity;
import com.hashStudio.nabdClone.ui.Magazine.MagazineFragment;
import com.hashStudio.nabdClone.ui.feed.FeedActivity;
import com.hashStudio.nabdClone.ui.feed.mostRead.MostReadFragment;
import com.hashStudio.nabdClone.ui.latestNews.LatestNewsFragment;
import com.hashStudio.nabdClone.ui.liveChannels.LiveChannelsFragment;
import com.hashStudio.nabdClone.ui.login.LoginActivity;
import com.hashStudio.nabdClone.ui.main2.Main2Activity;
import com.hashStudio.nabdClone.ui.myAccount.MyAccountFragment;
import com.hashStudio.nabdClone.ui.sources.SourceActivity;
import com.hashStudio.nabdClone.ui.splash.SplashActivity;
import com.hashStudio.nabdClone.ui.sports.SportFragment;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(CategoryActivity activity);

    void inject(SourceActivity activity);

    void inject(Main2Activity activity);

    void inject(LoginActivity activity);

    void inject(LiveChannelsFragment fragment);

    void inject(SplashActivity activity);

    void inject(FeedActivity activity);

    void inject(SportFragment fragment);

    void inject(MagazineFragment fragment);

    void inject(MyAccountFragment fragment);

    void inject(LatestNewsFragment fragment);

    void inject(MostReadFragment fragment);
}
