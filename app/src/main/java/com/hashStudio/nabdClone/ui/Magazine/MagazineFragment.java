/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.hashStudio.nabdClone.ui.Magazine;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.hashStudio.nabdClone.ui.Category.CategoryActivity;
import com.hashStudio.nabdClone.R;
import com.hashStudio.nabdClone.di.component.ActivityComponent;
import com.hashStudio.nabdClone.ui.base.BaseFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class MagazineFragment extends BaseFragment implements MagazineMvpView {

    public static final String TAG = "MagazineFragment";

    @Inject
    MagazineMvpPresenter<MagazineMvpView> mPresenter;
    @BindView(R.id.sportMagazineLayout)
    LinearLayout sportMagazineLayout;
    @BindView(R.id.healthMagazineLayout)
    LinearLayout healthMagazineLayout;
    @BindView(R.id.techMagazineLayout)
    LinearLayout techMagazineLayout;
    @BindView(R.id.entertainmentMagazineLayout)
    LinearLayout entertainmentMagazineLayout;
    @BindView(R.id.scienceMagazineLayout)
    LinearLayout scienceMagazineLayout;
    Unbinder unbinder;


    public static MagazineFragment newInstance() {
        Bundle args = new Bundle();
        MagazineFragment fragment = new MagazineFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_magazine, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }

        Log.d(TAG, "onCreateView: ");
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    protected void setUp(View view) {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }


    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick(R.id.sportMagazineLayout)
    public void onSportMagazineLayoutClicked() {
        Intent i = new Intent(getContext() , CategoryActivity.class );
        i.putExtra("title" , "نبض الرياضة");
        i.putExtra("category" , 0);
        startActivity(i);
    }

    @OnClick(R.id.healthMagazineLayout)
    public void onHealthMagazineLayoutClicked() {
        Intent i = new Intent(getContext() , CategoryActivity.class );
        i.putExtra("title" , "نبض الصحة");
        i.putExtra("category" , 1);
        startActivity(i);
    }

    @OnClick(R.id.techMagazineLayout)
    public void onTechMagazineLayoutClicked() {
        Intent i = new Intent(getContext() , CategoryActivity.class );
        i.putExtra("title" , "نبض الكنولوجيا");
        i.putExtra("category" , 2);
        startActivity(i);
    }

    @OnClick(R.id.entertainmentMagazineLayout)
    public void onEntertainmentMagazineLayoutClicked() {
        Intent i = new Intent(getContext() , CategoryActivity.class );
        i.putExtra("title" , "نبض الترفيه");
        i.putExtra("category" , 3);
        startActivity(i);
    }

    @OnClick(R.id.scienceMagazineLayout)
    public void onScienceMagazineLayoutClicked() {
        Intent i = new Intent(getContext() , CategoryActivity.class );
        i.putExtra("title" , "نبض المعرفة");
        i.putExtra("category" , 4);
        startActivity(i);
    }
}
