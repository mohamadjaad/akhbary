package com.hashStudio.nabdClone.ui.Category;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ImageButton;
import android.widget.TextView;

import com.hashStudio.nabdClone.R;
import com.hashStudio.nabdClone.data.network.model.LatestNewsResponse;
import com.hashStudio.nabdClone.ui.base.BaseActivity;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CategoryActivity extends BaseActivity implements CategoryMvpView {
    private static final String TAG = "CategoryActivity";
    @Inject
    CategoryMvpPresenter<CategoryMvpView> mPresenter;

    @Inject
    LinearLayoutManager mLayoutManager;

    @Inject
    CategoryAdapter mCategoryAdapter;

    @BindView(R.id.nav_back_btn)
    ImageButton navBackBtn;
    @BindView(R.id.categoryRecycleView)
    RecyclerView categoryRecycleView;
    @BindView(R.id.tv_title)
    TextView tvTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        getActivityComponent().inject(this);
        mPresenter.onAttach(this);
        setUnBinder(ButterKnife.bind(this));

        setUp();
    }

    @Override
    protected void setUp() {

        Intent intent = getIntent();
        String title =  intent.getStringExtra("title");
        int i = intent.getIntExtra("category" , 0 );
        tvTitle.setText(title);
        Log.d(TAG, "setUp: " + title + "  " + i );
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        categoryRecycleView.setLayoutManager(mLayoutManager);
        categoryRecycleView.setItemAnimator(new DefaultItemAnimator());
        categoryRecycleView.setAdapter(mCategoryAdapter);

        mPresenter.onViewPrepared(i);
    }

    @Override
    public void updateBlog(List<LatestNewsResponse.ArticlesBean> beanList) {
        mCategoryAdapter.addItems(beanList);
    }

    @OnClick(R.id.nav_back_btn)
    public void onNavBackBtnClicked() {
        finish();
    }

}
