/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.hashStudio.nabdClone.ui.sources;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import com.hashStudio.nabdClone.R;
import com.hashStudio.nabdClone.ui.base.BaseActivity;
import java.util.List;
import javax.inject.Inject;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.toptas.rssconverter.RssItem;

public class SourceActivity extends BaseActivity implements
        SourceMvpView, SourceAdapter.Callback {

    private static final String TAG = "SourceActivity";

    @Inject
    SourceMvpPresenter<SourceMvpView> mPresenter;

    @Inject
    SourceAdapter sourceAdapter;

    @Inject
    LinearLayoutManager mLayoutManager;

    @BindView(R.id.nav_back_btn)
    ImageButton navBackBtn;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.categoryRecycleView)
    RecyclerView recycleView;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        getActivityComponent().inject(this);
        mPresenter.onAttach(this);
        setUnBinder(ButterKnife.bind(this));
        Log.d(TAG, "onCreate: ");

        setUp();
    }

    @Override
    public void onBlogEmptyViewRetryClick() {

    }

    @Override
    public void updateBlog(List<RssItem> articleList) {
        sourceAdapter.addItems(articleList);
        sourceAdapter.notifyDataSetChanged();
    }


    @Override
    protected void setUp() {

        Log.d( TAG , "setUp: ");
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recycleView.setLayoutManager(mLayoutManager);
        recycleView.setItemAnimator(new DefaultItemAnimator());

        recycleView.setAdapter(sourceAdapter);
        Intent intent = getIntent();
        String source = intent.getStringExtra("source");
        String title = intent.getStringExtra("title");
        tvTitle.setText(title);

        mPresenter.onViewPrepared( source );
    }

    @OnClick({R.id.nav_back_btn, R.id.categoryRecycleView})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.nav_back_btn:
                finish();
                break;
            case R.id.categoryRecycleView:
                break;
        }
    }
}
