/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.hashStudio.nabdClone.ui.myAccount;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;
import com.hashStudio.nabdClone.R;
import com.hashStudio.nabdClone.di.component.ActivityComponent;
import com.hashStudio.nabdClone.ui.RssSources;
import com.hashStudio.nabdClone.ui.base.BaseFragment;
import com.hashStudio.nabdClone.ui.login.LoginActivity;
import com.hashStudio.nabdClone.ui.sources.SourceActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;


public class MyAccountFragment extends BaseFragment implements MyAccountMvpView  {

    public static final String TAG = "MagazineFragment";

    @Inject
    MyAccountMvpPresenter<MyAccountMvpView> mPresenter;
    @BindView(R.id.signout)
    Button signout;
    Unbinder unbinder;
    @BindView(R.id.loginButton)
    Button loginButton;
    @BindView(R.id.signUpButton)
    Button signUpButton;
    @BindView(R.id.loginLayout)
    LinearLayout loginLayout;
    @BindView(R.id.profileName)
    TextView profileName;
    @BindView(R.id.profileImageView)
    CircleImageView profileImageView;
    @BindView(R.id.profileLayout)
    LinearLayout profileLayout;
    @BindView(R.id.tv_fontSize)
    TextView tvFontSize;
    @BindView(R.id.tv_sources)
    TextView tvNotification;

    public static MyAccountFragment newInstance() {
        Bundle args = new Bundle();
        MyAccountFragment fragment = new MyAccountFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_account, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }

        Log.d(TAG, "onCreateView: ");
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    protected void setUp(View view) {
        mPresenter.getUserData();

    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.signout)
    public void onViewClicked() {
        FirebaseAuth.getInstance().signOut();
        LoginManager.getInstance().logOut();
        Intent intent = LoginActivity.getStartIntent(getContext());
        startActivity(intent);
        getActivity().finish();

    }

    @OnClick({R.id.loginButton, R.id.signUpButton})
    public void onViewClicked(View view) {
        Intent intent = LoginActivity.getStartIntent(getContext());
        switch (view.getId()) {
            case R.id.loginButton:
                startActivity(intent);
                getActivity().finish();
                break;
            case R.id.signUpButton:
                startActivity(intent);
                getActivity().finish();
                break;
        }
    }

    @Override
    public void showLoginLayout() {
        loginLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoginLayout() {
        loginLayout.setVisibility(View.GONE);
    }

    @Override
    public void showProfileLayout() {
        profileLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProfileLayout() {
        profileLayout.setVisibility(View.GONE);
    }

    @Override
    public void updateProfile(String name, Uri photoUri) {
        profileName.setText(name);

        Glide.with(this)
                .load(photoUri)
                //.override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                .asBitmap()

                .format(DecodeFormat.ALWAYS_ARGB_8888)
                .into(profileImageView)
        ;

    }

    @Override
    public void showSignOut() {
        signout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideSignOut() {
        signout.setVisibility(View.INVISIBLE);
    }


    @OnClick(R.id.tv_fontSize)
    public void onTvFontSizeClicked() {
        DialogFragment dialog = new FontSizeDialog();
        dialog.show(getFragmentManager() , "font dialog");
    }

    @OnClick(R.id.tv_sources)
    public void onTvNotificationClicked() {
        startActivity(new Intent(getActivity() , RssSources.class));
    }

}

