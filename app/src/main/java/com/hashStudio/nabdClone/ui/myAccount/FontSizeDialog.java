package com.hashStudio.nabdClone.ui.myAccount;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import com.hashStudio.nabdClone.R;

public class FontSizeDialog extends DialogFragment {

    public interface FontSizeListener {
        public void onDialogSmallClick(DialogFragment dialog);
        public void onDialogMediumClick(DialogFragment dialog);
        public void onDialogBigClick(DialogFragment dialog);

    }

    FontSizeListener listener ;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            listener = (FontSizeListener) context ;
        }catch (ClassCastException e){

            e.printStackTrace();
            throw new ClassCastException(getActivity().toString()
                    + " must implement FontSizeListener");

        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.pick_font_size)
                .setItems(R.array.font_size_array, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int i ) {
                        // The 'which' argument contains the index position
                        // of the selected item
                        switch (i){
                            case 0 : //small
                                listener.onDialogSmallClick(FontSizeDialog.this);
                                break;
                            case  1 : //medium
                                listener.onDialogMediumClick(FontSizeDialog.this);

                                break;
                            case  2 : //big
                                listener.onDialogBigClick(FontSizeDialog.this);

                                break;
                        }
                    }
                });

        return builder.create();
    }
}
