/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.hashStudio.nabdClone.ui.latestNews;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hashStudio.nabdClone.R;
import com.hashStudio.nabdClone.data.network.model.LatestNewsResponse;
import com.hashStudio.nabdClone.ui.base.BaseViewHolder;
import com.hashStudio.nabdClone.utils.AppLogger;

import org.joda.time.DateTime;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Janisharali on 25-05-2017.
 */

public class LatestNewsAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;

    int fontSize ;

    public void setTextSize(int i ){
        fontSize = i ;
    }

    private Callback mCallback;
    private List<LatestNewsResponse.ArticlesBean> mArticleResponseList;

    public LatestNewsAdapter(List<LatestNewsResponse.ArticlesBean> articleResponseList) {
        mArticleResponseList = articleResponseList;
    }

    public void setCallback(Callback callback) {
        mCallback = callback;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                return new ViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_article_view, parent, false));
            case VIEW_TYPE_EMPTY:
            default:
                return new EmptyViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_view, parent, false));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mArticleResponseList != null && mArticleResponseList.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public int getItemCount() {
        if (mArticleResponseList != null && mArticleResponseList.size() > 0) {
            return mArticleResponseList.size();
        } else {
            return 1;
        }
    }

    public void addItems(List<LatestNewsResponse.ArticlesBean> articlesBeanList) {

        mArticleResponseList.addAll(articlesBeanList);
        notifyDataSetChanged();

    }

    public interface Callback {
        void onArticlesBeanEmptyViewRetryClick();
    }

    public class ViewHolder extends BaseViewHolder {

        @BindView(R.id.cover_image_view)
        ImageView coverImageView;
        @BindView(R.id.title_text_view)
        TextView titleTextView;
        @BindView(R.id.description_text_view)
        TextView descriptionTextView;
        @BindView(R.id.source_text_view)
        TextView sourceTextView;
        @BindView(R.id.date_text_view)
        TextView dateTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        protected void clear() {
            coverImageView.setImageDrawable(null);
            titleTextView.setText("");
            descriptionTextView.setText("");
        }

        public void onBind(int position) {
            super.onBind(position);

            final LatestNewsResponse.ArticlesBean articlesBean = mArticleResponseList.get(position);

            if (articlesBean.getUrlToImage() != null) {
                Glide.with(itemView.getContext())
                        .load(articlesBean.getUrlToImage())
                        .asBitmap()
                        .centerCrop()
                        .into(coverImageView);
            }

            if (articlesBean.getTitle() != null) {
                titleTextView.setText(articlesBean.getTitle());
                //set textSize

                titleTextView.setTextSize(fontSize);
            }


            if (articlesBean.getPublishedAt() != null) {
                DateTime dt = new DateTime(articlesBean.getPublishedAt());
                String date = (String) DateFormat.format("EEE, d MMM yyyy HH:mm", dt.toDate().getTime());

                dateTextView.setText(date);
            }

            if (articlesBean.getDescription() != null) {
                descriptionTextView.setText(articlesBean.getDescription());
            }

            if (articlesBean.getSource() != null) {
                sourceTextView.setText(articlesBean.getSource().getName());
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (articlesBean.getUrl() != null) {
                        try {
                            Intent intent = new Intent();
                            intent.setAction(Intent.ACTION_VIEW);
                            intent.addCategory(Intent.CATEGORY_BROWSABLE);
                            intent.setData(Uri.parse(articlesBean.getUrl()));
                            itemView.getContext().startActivity(intent);
                        } catch (Exception e) {
                            AppLogger.d("url error");
                        }
                    }
                }
            });
        }
    }

    public class EmptyViewHolder extends BaseViewHolder {

        @BindView(R.id.btn_retry)
        Button retryButton;

        @BindView(R.id.tv_message)
        TextView messageTextView;

        public EmptyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        protected void clear() {

        }

        @OnClick(R.id.btn_retry)
        void onRetryClick() {
            if (mCallback != null)
                mCallback.onArticlesBeanEmptyViewRetryClick();
        }
    }
}
