/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.hashStudio.nabdClone.ui.latestNews;

import com.androidnetworking.error.ANError;
import com.hashStudio.nabdClone.data.DataManager;
import com.hashStudio.nabdClone.data.network.model.LatestNewsResponse;
import com.hashStudio.nabdClone.ui.base.BasePresenter;
import com.hashStudio.nabdClone.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

/**
 * Created by janisharali on 25/05/17.
 */

public class LatestNewsPresenter<V extends LatestNewsMvpView> extends BasePresenter<V>
        implements LatestNewsMvpPresenter<V> {

    @Inject
    public LatestNewsPresenter(DataManager dataManager,
                               SchedulerProvider schedulerProvider,
                               CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onViewPrepared(int page) {
        //getMvpView().showLoading();
        getCompositeDisposable().add(getDataManager()
                .getLatestNewsApiCall(page)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<LatestNewsResponse>() {
                    @Override
                    public void accept(@NonNull LatestNewsResponse latestNewsResponse)
                            throws Exception {
                        if (latestNewsResponse != null && latestNewsResponse.getArticles() != null) {
                            getMvpView().updateBlog(latestNewsResponse.getArticles());
                        }
                        getMvpView().hideLoading();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable)
                            throws Exception {
                        if (!isViewAttached()) {
                            return;
                        }

                        getMvpView().hideLoading();

                        // handle the error here
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }
                }));
    }

    @Override
    public void onSearchQuery(String search) {
        //getMvpView().showLoading();
        getCompositeDisposable().add(getDataManager()
                .getSearchApiCall(search)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<LatestNewsResponse>() {
                    @Override
                    public void accept(@NonNull LatestNewsResponse latestNewsResponse)
                            throws Exception {
                        if (latestNewsResponse != null && latestNewsResponse.getArticles() != null) {
                            getMvpView().updateBlog(latestNewsResponse.getArticles());
                        }
                        getMvpView().hideLoading();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable)
                            throws Exception {
                        if (!isViewAttached()) {
                            return;
                        }

                        getMvpView().hideLoading();
                        getMvpView().hideKeyboard();

                        // handle the error here
                        if (throwable instanceof ANError) {
                            ANError anError = (ANError) throwable;
                            handleApiError(anError);
                        }
                    }
                }));
    }

    @Override
    public int getFontSize() {
        return getDataManager().getFontSize();
    }
}
