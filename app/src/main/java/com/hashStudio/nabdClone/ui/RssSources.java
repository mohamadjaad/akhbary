package com.hashStudio.nabdClone.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hashStudio.nabdClone.R;
import com.hashStudio.nabdClone.ui.base.BaseActivity;
import com.hashStudio.nabdClone.ui.sources.SourceActivity;
import com.hashStudio.nabdClone.utils.AppConstants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RssSources extends BaseActivity {

    @BindView(R.id.youmSabea)
    TextView youm7;
    @BindView(R.id.arab2day)
    TextView arab2day;
    @BindView(R.id.masrawy)
    TextView masrawy;
    @BindView(R.id.elmasryelyoum)
    TextView elmasryelyoum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rss_sources);
        ButterKnife.bind(this);
    }

    @Override
    protected void setUp() {

    }

    @OnClick({R.id.youmSabea, R.id.arab2day, R.id.masrawy, R.id.elmasryelyoum})
    public void onViewClicked(View view) {
        Intent intent = new Intent(this , SourceActivity.class);

        switch (view.getId()) {
            case R.id.youmSabea:
                intent.putExtra("source" , AppConstants.youm7);
                intent.putExtra("title" , "اليوم السابع");
                break;
            case R.id.arab2day:
                intent.putExtra("source" , AppConstants.arab2day);
                intent.putExtra("title" , "عرب توداي");
                break;
            case R.id.masrawy:
                intent.putExtra("source" , AppConstants.masrawy);
                intent.putExtra("title" , "مصراوى");
                break;
            case R.id.elmasryelyoum:
                intent.putExtra("source" , AppConstants.elmasryElyoum);
                intent.putExtra("title" , "المصرى اليوم");
                break;
        }

        startActivity(intent);
    }
}
