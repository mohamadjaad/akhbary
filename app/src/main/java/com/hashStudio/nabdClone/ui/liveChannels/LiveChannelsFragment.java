/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.hashStudio.nabdClone.ui.liveChannels;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.hashStudio.nabdClone.R;
import com.hashStudio.nabdClone.data.network.model.BlogResponse;
import com.hashStudio.nabdClone.di.component.ActivityComponent;
import com.hashStudio.nabdClone.ui.base.BaseFragment;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class LiveChannelsFragment extends BaseFragment implements
        LiveChannelsMvpView, LiveChannelsAdapter.Callback {

    private static final String TAG = "LiveChannelsFragment";

    @Inject
    LiveChannelsMvpPresenter<LiveChannelsMvpView> mPresenter;

    @Inject
    LiveChannelsAdapter mBlogAdapter;

    @Inject
    LinearLayoutManager mLayoutManager;
    @BindView(R.id.iv_arabia)
    ImageView ivArabia;
    Unbinder unbinder;
    @BindView(R.id.iv_skyNews)
    ImageView ivSkyNews;
    @BindView(R.id.iv_bein)
    ImageView ivBein;
    @BindView(R.id.iv_bbc)
    ImageView ivBbc;
    @BindView(R.id.iv_hadas)
    ImageView ivHadas;
    @BindView(R.id.iv_dmc)
    ImageView ivDmc;

//    @BindView(R.id.blog_recycler_view)
//    RecyclerView mRecyclerView;

    public static LiveChannelsFragment newInstance() {
        Bundle args = new Bundle();
        LiveChannelsFragment fragment = new LiveChannelsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_resources, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
            mBlogAdapter.setCallback(this);
        }
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    protected void setUp(View view) {
//        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
//        mRecyclerView.setLayoutManager(mLayoutManager);
//        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
//        mRecyclerView.setAdapter(mBlogAdapter);

        mPresenter.onViewPrepared();
    }

    @Override
    public void onBlogEmptyViewRetryClick() {

    }

    @Override
    public void updateBlog(List<BlogResponse.Blog> blogList) {
        mBlogAdapter.addItems(blogList);
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
        unbinder.unbind();
    }

    private void openChannel(String id ){
        Intent appIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("vnd.youtube:" + id));
        appIntent.putExtra("force_fullscreen",true);

        Intent webIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("https://www.youtube.com/watch?v=" + id ));
        webIntent.putExtra("force_fullscreen",true);

        try {
            getContext().startActivity(appIntent);
        } catch (ActivityNotFoundException ex) {
            getContext().startActivity(webIntent);
        }
    }


    @OnClick({R.id.iv_skyNews, R.id.iv_bein, R.id.iv_bbc, R.id.iv_hadas, R.id.iv_dmc , R.id.iv_arabia})
    public void onViewClicked(View view) {

        // bbc           https://www.youtube.com/watch?v=HxI2TxhhS9A
        //sada elbalad   https://www.youtube.com/watch?v=Rf0yp2eTBXk
        //dmc            https://www.youtube.com/watch?v=n32Os1mVZgE
        //alhadas        https://www.youtube.com/watch?v=mwPhTThS9uo
        //bein  https://www.youtube.com/watch?v=QQLv15h7ve4
        //skynews  https://www.youtube.com/watch?v=t_WmiOt-034

        switch (view.getId()) {
            case R.id.iv_skyNews:
                openChannel("t_WmiOt-034");
                break;
            case R.id.iv_bein:
                openChannel("QQLv15h7ve4");
                break;
            case R.id.iv_bbc:
                openChannel("HxI2TxhhS9A");
                break;
            case R.id.iv_hadas:
                openChannel("mwPhTThS9uo");
                break;
            case R.id.iv_dmc:
                openChannel("n32Os1mVZgE");
                break;
            case R.id.iv_arabia:
                openChannel("uFcnqm6RmM8");
                break;
        }
    }
}
