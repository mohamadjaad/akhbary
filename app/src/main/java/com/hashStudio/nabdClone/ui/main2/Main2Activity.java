package com.hashStudio.nabdClone.ui.main2;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.MenuItem;

import com.hashStudio.nabdClone.R;
import com.hashStudio.nabdClone.ui.Magazine.MagazineFragment;
import com.hashStudio.nabdClone.ui.base.BaseActivity;
import com.hashStudio.nabdClone.ui.latestNews.LatestNewsFragment;
import com.hashStudio.nabdClone.ui.liveChannels.LiveChannelsFragment;
import com.hashStudio.nabdClone.ui.myAccount.FontSizeDialog;
import com.hashStudio.nabdClone.ui.myAccount.MyAccountFragment;
import com.hashStudio.nabdClone.ui.sports.SportFragment;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Main2Activity extends BaseActivity implements Main2MvpView, FontSizeDialog.FontSizeListener {
    private static final String TAG = "Main2Activity";
    @Inject
    Main2MvpPresenter<Main2MvpView> mPresenter;
    @BindView(R.id.navigation)
    BottomNavigationViewEx navigation;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        getActivityComponent().inject(this);

        setUnBinder(ButterKnife.bind(this));

        mPresenter.onAttach(this);

        setUp();

        Log.d(TAG, "onCreate: ");

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragmentContainer, new LatestNewsFragment())
                .commit();

    }

    @TargetApi(Build.VERSION_CODES.O)
    @Override
    protected void setUp() {

        navigation.setItemHorizontalTranslationEnabled(false);
        navigation.clearAnimation();
        navigation.setTextSize(12);
        navigation.enableShiftingMode(false);
        navigation.enableItemShiftingMode(false);

    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            FragmentManager manager = getSupportFragmentManager();
            switch (item.getItemId()) {
                case R.id.menu_home_news:
                    manager.beginTransaction().replace(R.id.fragmentContainer, new LatestNewsFragment()).commit();
                    return true;
                case R.id.menu_sports:
                    manager.beginTransaction().replace(R.id.fragmentContainer, new SportFragment()).commit();

                    return true;
                case R.id.menu_sources:
                    manager.beginTransaction().replace(R.id.fragmentContainer, new LiveChannelsFragment()).commit();

                    return true;
                case R.id.menu_magazines:
                    manager.beginTransaction().replace(R.id.fragmentContainer, new MagazineFragment()).commit();

                    return true;

                case R.id.menu_settings:
                    manager.beginTransaction().replace(R.id.fragmentContainer, new MyAccountFragment()).commit();

                    return true;
            }
            return false;
        }
    };

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, Main2Activity.class);
        return intent;
    }

    @Override
    public void onDialogSmallClick(DialogFragment dialog) {
        //set font size to small
        mPresenter.setFontSize(15);
    }

    @Override
    public void onDialogMediumClick(DialogFragment dialog) {
        //set font size to medium
        mPresenter.setFontSize(18);
    }

    @Override
    public void onDialogBigClick(DialogFragment dialog) {
        //set font size to big
        mPresenter.setFontSize(22);
    }
}
