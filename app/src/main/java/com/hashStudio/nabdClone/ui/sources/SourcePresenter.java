/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.hashStudio.nabdClone.ui.sources;

import android.util.Log;
import com.hashStudio.nabdClone.data.DataManager;
import com.hashStudio.nabdClone.data.network.RssService;
import com.hashStudio.nabdClone.ui.base.BasePresenter;
import com.hashStudio.nabdClone.utils.rx.SchedulerProvider;
import java.util.List;
import javax.inject.Inject;
import io.reactivex.disposables.CompositeDisposable;
import me.toptas.rssconverter.RssConverterFactory;
import me.toptas.rssconverter.RssFeed;
import me.toptas.rssconverter.RssItem;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public class SourcePresenter<V extends SourceMvpView> extends BasePresenter<V>
        implements SourceMvpPresenter<V> {

    private static final String TAG = "SourcePresenter";

    @Inject
    public SourcePresenter(DataManager dataManager,
                           SchedulerProvider schedulerProvider,
                           CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onViewPrepared(String source) {

        Log.d(TAG, "onViewPrepared: ");

        getMvpView().showLoading();
        String url = "https://www.masrawy.com/";

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(RssConverterFactory.Companion.create())
                .build();

        RssService service = retrofit.create(RssService.class);

        Call<RssFeed> call = null ;

        //check source
        System.out.println(source);

        switch (source){

            case "youm7" :
                call = service.getRssYoumSabea();
                break;
            case "masrawy" :
                call = service.getRssMasrawy();
                break;
            case "arab2day" :
                call = service.getRssEgyToday();
                break;
            case "elmogaz" :
                call = service.getRssMogaz();
                break;
            case "elmasryElyoum" :
                call = service.getElmasryElyoum();
                break;
        }


        call.enqueue(new Callback<RssFeed>() {
            @Override
            public void onResponse(Call<RssFeed> call, Response<RssFeed> response) {

                System.out.println( response.body() ) ;
                List<RssItem> list = response.body().getItems();
                getMvpView().updateBlog(list);

            }

            @Override
            public void onFailure(Call<RssFeed> call, Throwable t) {

                t.printStackTrace();
                Log.d(TAG, "onFailure: ");

            }
        });

        getMvpView().hideLoading();

    }
}
